<?php

return [
    'format'           => 'A4', // See https://mpdf.github.io/paging/page-size-orientation.html
    'author'           => 'John Doe',
    'subject'          => 'This Document will explain the whole universe.',
    'keywords'         => 'PDF, Laravel, Package, Peace', // Separate values with comma
    'creator'          => 'Laravel Pdf',
    'display_mode'     => 'fullpage',
	'mode'                  => 'utf-8',
    'font_path' => base_path('resources/fonts/'),
    'font_data' => [
        'Shabnam' => [
            'R'  => 'Shabnam.ttf',    // regular font
            'B'  => 'Shabnam.ttf',          // optional: bold font
            'I'  => 'Shabnam.ttf',    // optional: italic font
            'BI' => 'Shabnam.ttf',           // optional: bold-italic font
            'useOTL' => 0xFF,
            'useKashida' => 75,
        ],
        // ...add as many as you want.
    ]

];

//return [
//    'format'           => 'A4', // See https://mpdf.github.io/paging/page-size-orientation.html
//    'author'           => 'John Doe',
//    'subject'          => 'This Document will explain the whole universe.',
//    'keywords'         => 'PDF, Laravel, Package, Peace', // Separate values with comma
//    'creator'          => 'Laravel Pdf',
//    'display_mode'     => 'fullpage'
//];
//    'format'                => 'A4',
//    'defaultFontSize'       => '144',
//    'defaultFont'           => 'Shabnam',
//    'marginLeft'            => 10,
//    'marginRight'           => 10,
//    'marginTop'             => 10,
//    'marginBottom'          => 10,
//    'marginHeader'          => 0,
//    'marginFooter'          => 0,
//    'orientation'           => 'P',
//    'title'                 => 'Laravel PDF',
//    'author'                => '',
//    'watermark'             => '',
//    'showWatermark'         => false,
//    'watermarkFont'         => 'Shabnam',
//    'SetDisplayMode'        => 'fullpage',
//    'watermarkTextAlpha'    => 0.1
